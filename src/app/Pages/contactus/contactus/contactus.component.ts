import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthService } from '../../../core/auth/auth.service';
import { AboutusService } from '../../../Core/Services/aboutus.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  data: any;
  data1: any;

  constructor(
    private about:AboutusService,
    private authService: AuthService,
    ) {
   }

  ngOnInit() {
    debugger
    this.about.getData()
    .pipe(map(res => {
      this.data= res;
    })).subscribe();
    console.log(this.data); 
  }
}
