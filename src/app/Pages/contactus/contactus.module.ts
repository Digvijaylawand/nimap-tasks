import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactusComponent } from './contactus/contactus.component';
import { RouterModule, Routes } from '@angular/router';
import {MatIconModule} from '@angular/material/icon'
const routes: Routes = [
  {
    path: '', component: ContactusComponent,
  }
];

@NgModule({
  declarations: [ContactusComponent],
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule.forChild(routes)
  ]
})
export class ContactusModule { }
