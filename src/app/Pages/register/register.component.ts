import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, map } from 'rxjs/operators';
import { AuthService } from '../../core/auth/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  loginSuccessfull: boolean;
  registerdata: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,    
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      token: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  this.getRegisterdata();
  }
  get f() { return this.registerForm.controls; }
  getRegisterdata() {
    this.authService.getregister().pipe(
      map(res => {
        this.registerdata = res;
        console.log(this.registerdata);
      })).subscribe();
  };
  
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.registerdata.forEach((value: any) => {
      console.log(this.registerForm.value.email, value.email, this.registerForm.value.password, value.password)
      if (this.registerForm.value.email === value.email && this.registerForm.value.password === value.password) {
        this.loginSuccessfull = true;
      }
    });

    if (this.loginSuccessfull) {
      //this.showError();
    }
    else {
      this.loading = true;
      this.registerForm.controls['token'].setValue('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiTml6YW0iLCJVc2VySWQiOjUsIlJvbGVJZCI6MSwianRpIjoiYTI1MDlkNjAtNDBiMy00MzBiLTkxYWQtNjk4N2IzMjkwMTFlIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiVXNlciIsImV4cCI6MTYzNTg0NDI2OSwiaXNzIjoiaHR0cDovLzk1LjExMS4yMzAuMzoxNzAwL0FQSS9BUEkvIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0MzAwLyJ9.wuGspVoYLWyVIGeiCo5hErcGnM1NJ8qVTJ_K6zbdxB0');
      this.authService.register(this.registerForm.value).pipe(map(res => {
        //this.showSuccess();
        this.router.navigate(['/login']);
      })).subscribe();
    }



  }
}
