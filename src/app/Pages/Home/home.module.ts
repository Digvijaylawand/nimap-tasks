import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import { EditComponent } from './edit/edit.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorchangeDirective } from 'src/app/Core/Directives/colorchange.directive';
import { CustomstringPipe } from 'src/app/Core/Services/customstring.pipe';
const routes: Routes = [
  {
    path: '', component: HomeComponent,
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    CustomstringPipe,
    ColorchangeDirective,
    EditComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule { }
