import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { send } from 'process';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
@Input() sendData;
@Output() sendDataToParent = new EventEmitter();
registerForm: FormGroup;
submitted = false;
  tempData: any;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      id: [''],
      username: ['', Validators.required],
      email: ['', Validators.required],
      gender: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(6)]]
  });
  // this.tempData =[{
  //   "username":this.sendData.username ,
  //   "gender" : this.sendData.gender,
  //   "phone" : this.sendData.phone
  // }
  // ]
  // console.log(this.sendData)
  // this.registerForm.patchValue({"username":"Digvijay" ,
  // "gender" : "Male"})
  // console.log(this.sendData);
  }
  get f() { return this.registerForm.controls; }
  // ngOnChanges(){
  //   console.log(this.sendData);
  // }
  dataSendToParent(){
    
  }
  dataTemp()
  {
    this.sendData= 43;
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.registerForm.value.id=this.sendData.id;
    var data=[
      this.registerForm.value
    ]

    console.log(this.registerForm.value);
    this.sendDataToParent.emit(this.registerForm.value);
  }
}
