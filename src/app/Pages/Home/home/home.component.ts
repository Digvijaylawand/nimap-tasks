import { Component, OnInit } from '@angular/core';
import {MatIconModule} from '@angular/material/icon'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  dummy="This page can be accessed only by administrators";
  data=[
    { id: 1, username: "Digvijay", email: "Digvijay@gmail.com", gender: "Male",phone:9909009909},

    { id: 2, username: "Ajay", email: "Ajay@gmail.com", gender: "Male",phone:9909009909 },

    { id: 3, username: "Vijay", email: "Vijay@gmail.com", gender: "Male",phone:9909009909 },

    { id: 4, username: "Rakesh", email: "Rakesh@gmail.com", gender: "Male",phone:9909009909},

    { id: 5, username: "Praful", email: "Praful@gmail.com", gender: "Male",phone:9909009909 },

    { id: 6, username: "Akshay", email: "test@gmail.com", gender: "Male",phone:9909009909 },
  ]
  sendData=[];
  fromParent: any;
  num=1;
  interval: any;
  timeLeft: number;
  timerdata: string;
  constructor() { }

  ngOnInit() {
    this.startTimer()
  }
  // ngOnChanges()
  // {
  //     this.sendData = this.sendData + 1;
  //     console.log(this.sendData)
  // }
  updateData()
  {
    
      console.log(this.sendData)
  }
  openchild(data)
  {
      this.sendData = data;
  }
  dataFromParent(fromParent)
  {
     this.fromParent=fromParent;
     var objIndex = this.data.findIndex((obj => obj.id == fromParent.id)); 
     console.log(objIndex) ;
     this.data[objIndex].username=fromParent.username;
     this.data[objIndex].email=fromParent.email;
     this.data[objIndex].gender=fromParent.gender;
     this.data[objIndex].phone=fromParent.phone; 
     console.log(this.data);    //this.data=fromParent;
     console.log(fromParent.phone);
  }
  
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft < 60) {
        this.timeLeft++;
      }
      else if (this.timeLeft === 60) {
        this.timeLeft = 0;
         this.timerdata+="Hello  ";
        //this.router.navigate(['/pages/readMessage']);
      }
      else {
        this.timeLeft = 60;
      }
    }, 100)

  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
