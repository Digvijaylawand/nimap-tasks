import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, map } from 'rxjs/operators';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  loginSuccessfull: any;
  registerdata: any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  this.getRegisterdata();  
  }
  get f() { return this.loginForm.controls; }

  getRegisterdata() {
    this.authService.getregister().pipe(
      map(res => {
        this.registerdata = res;
        console.log(this.registerdata);
      })).subscribe();
  };

  submit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    this.registerdata.forEach((value: any) => {
      console.log(this.loginForm.value.username, value.email, this.loginForm.value.password, value.password)
      if (this.loginForm.value.username === value.email && this.loginForm.value.password === value.password) {
        localStorage.setItem('token', (value.token));
        this.router.navigate(['/'], { relativeTo: this.route });
        this.loginSuccessfull = true;
      }
    });
    if (this.loginSuccessfull) {
      //this.showSuccess();

      window.location.reload();
      this.router.navigate(['/']);
    }
  }
}
