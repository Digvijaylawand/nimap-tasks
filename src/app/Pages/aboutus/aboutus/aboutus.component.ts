import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {
  displayedColumns = ['id', 'name', 'email', 'gender','phone'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  data=[
    { id: 1, username: "Digvijay", email: "Digvijay@gmail.com", gender: "Male",phone:9909009909},

    { id: 2, username: "Ajay", email: "Ajay@gmail.com", gender: "Male",phone:9909009909 },

    { id: 3, username: "Vijay", email: "Vijay@gmail.com", gender: "Male",phone:9909009909 },

    { id: 4, username: "Rakesh", email: "Rakesh@gmail.com", gender: "Male",phone:9909009909},

    { id: 5, username: "Praful", email: "Praful@gmail.com", gender: "Male",phone:9909009909 },

    { id: 6, username: "Akshay", email: "test@gmail.com", gender: "Male",phone:9909009909 },
  ]
  constructor() { 
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  toggleDarkTheme(): void {
    document.body.classList.toggle('dark-theme');
  }
}
