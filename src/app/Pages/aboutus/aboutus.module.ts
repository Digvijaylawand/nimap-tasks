import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutusComponent } from './aboutus/aboutus.component';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
  MatSortModule, MatTableModule } from "@angular/material";
import { RouterModule, Routes } from '@angular/router';
import { AboutusService } from 'src/app/Core/Services/aboutus.service';

  const routes: Routes = [
    {
      path: '', component: AboutusComponent,
    }
  ];
  

@NgModule({
  declarations: [AboutusComponent],
  imports: [
    CommonModule,
    MatInputModule, 
    MatPaginatorModule, 
    MatProgressSpinnerModule, 
    MatSortModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ],
  providers: [AboutusService]
})
export class AboutusModule { }
