import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './core/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'authentication';
  currentUser: any;
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }
  logout() {
    localStorage.removeItem('token')
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  onEvents(router){
   this.router.navigate([`${router}`]);
  }
}
