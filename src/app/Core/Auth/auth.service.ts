import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  constructor(private http: HttpClient)
 {this.currentUserSubject = new BehaviorSubject<any>((localStorage.getItem('token')));
  this.currentUser  = this.currentUserSubject.asObservable(); }
  
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }
  
  verifyCustomerLogin(data: any) {
    var headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post<any>(`${environment.baseUrl}api/Authenticate/login`, data, headers)
      .pipe(map(user => {
        const token = user.token;
        const decoded = btoa(token);
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('token', JSON.stringify(decoded));
        // console.log(user.token)
       
        this.currentUserSubject.next(user);
        return user;
      }));
  }
  
  getregister(){
    return this.http.get<any>(`${environment.baseUrl}user`)
      .pipe(map(user => {
        return user;
      }));
  }

  register(data: any) {
    return this.http.post<any>(`${environment.baseUrl}user`, data)
      .pipe(map(user => {
        return user;
      }));
  }

  logout(): Observable<any> {
    this.currentUserSubject.next(null);
    return this.http.post<any>(`/api/auth/logout`, {})
  }
  getData(){
    debugger
    var headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.get<any>(`${environment.baseUrl}user`,headers)
      .pipe(map(user => {
        return user;
      }));
  }
}
