import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appColorchange]'
})
export class ColorchangeDirective {

  constructor(eleRef:ElementRef) { 
    eleRef.nativeElement.style.color = '#00cc66';
    eleRef.nativeElement.style.backgroundColor = '#ccccff';
    eleRef.nativeElement.style.fontSize = '20px';
  }

}
