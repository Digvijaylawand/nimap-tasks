import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AboutusService {

  constructor(private http: HttpClient) { }

  getData(){
    debugger
    return this.http.get<any>(`${environment.baseUrl}table`)
      .pipe(map(res => {
        console.log(res);
        return res;
      }));
  }
}
